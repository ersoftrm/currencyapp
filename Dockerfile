FROM openjdk:17
COPY target/currencyapp*.jar currencyapp.jar
ENTRYPOINT ["java","-jar","/currencyapp.jar"]