package pl.ersoftrm.currencyapp;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.ersoftrm.currencyapp.infra.DomainEventPublisher;
import pl.ersoftrm.currencyapp.integrator.ExchangeRateFetchUseCase;
import pl.ersoftrm.currencyapp.integrator.FetchRateEvent;
import pl.ersoftrm.currencyapp.integrator.FetchRateEventPublisher;

@Component
public class CurrencyAppInitializer implements CommandLineRunner {

    private final ExchangeRateFetchUseCase fetchRateUseCase;
    private final DomainEventPublisher publisher;

    public CurrencyAppInitializer(ExchangeRateFetchUseCase fetchRateUseCase, @Qualifier("fetchRateEventPublisher") FetchRateEventPublisher publisher) {
        this.fetchRateUseCase = fetchRateUseCase;
        this.publisher = publisher;
    }

    @Override
    public void run(final String... args) {
        fetchRateUseCase.getUsdExchangeRate("usd").ifPresent(fetchRateDto -> {
            publisher.publish(new FetchRateEvent(fetchRateDto));
        });
    }
}
