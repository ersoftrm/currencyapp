package pl.ersoftrm.currencyapp.customer;

import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.Table;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import pl.ersoftrm.currencyapp.infra.BaseEntity;


@Entity
@Table(name = "customer")
@EntityListeners(AuditingEntityListener.class)
class Customer extends BaseEntity {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private Boolean isEnabled;

    Customer() {
    }

    Customer(final String firstName, final  String lastName, final String email, final String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.isEnabled=false;
    }

    CustomerDto toCustomerDto() {
        return CustomerDto.builder()
                .firstName(firstName)
                .lastName(lastName)
                .email(email)
                .password(password)
                .isEnabled(isEnabled)
                .build();
    }
   static Customer register(final String firstName, final String lastname, final String email, final String password) {
        return new Customer(firstName, lastname,email,password);
    }

    void activate(){
        isEnabled=true;
    }
}
