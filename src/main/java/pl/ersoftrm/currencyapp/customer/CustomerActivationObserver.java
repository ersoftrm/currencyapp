package pl.ersoftrm.currencyapp.customer;

public interface CustomerActivationObserver {
    void onCustomerActivated(CustomerIdDto dto);
}
