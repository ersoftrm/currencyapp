package pl.ersoftrm.currencyapp.customer;

import org.springframework.stereotype.Service;
import pl.ersoftrm.currencyapp.customer.port.CustomerActivationUseCase;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerActivationService implements CustomerActivationUseCase {
    private final CustomerRepository repository;
    private final List<CustomerActivationObserver> observers = new ArrayList<>();

    public CustomerActivationService(CustomerRepository repository) {
        this.repository = repository;
    }

    @Override
    public void registerObserver(final CustomerActivationObserver observer) {
        observers.add(observer);
    }
    @Override
    public void activateCustomer(final CustomerIdDto customerIdDto) {
       repository.findById(customerIdDto.getCustomerId()).ifPresent(Customer::activate);
       repository.flush();
       notifyObservers(customerIdDto);
    }

    @Override
    public void notifyObservers(final CustomerIdDto customerIdDto) {
        for (CustomerActivationObserver observer : observers) {
            observer.onCustomerActivated(customerIdDto);
        }
    }
}
