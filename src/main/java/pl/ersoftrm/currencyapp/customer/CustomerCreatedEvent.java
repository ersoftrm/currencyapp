package pl.ersoftrm.currencyapp.customer;

import pl.ersoftrm.currencyapp.infra.DomainEvent;

import java.time.Instant;

public class CustomerCreatedEvent implements DomainEvent<CustomerIdDto> {

    private final Instant occurredOn;
    private final CustomerIdDto dto;
    public CustomerCreatedEvent(final CustomerIdDto dto) {
        this.occurredOn = Instant.now();
        this.dto = dto;
    }
    @Override
    public Instant occurrenceTime() {
        return occurredOn;
    }

    @Override
    public CustomerIdDto getEventObject() {
        return dto;
    }
}
