package pl.ersoftrm.currencyapp.customer;

import lombok.*;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
@Getter
public class CustomerDto {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String token;
    @Setter
    private Boolean isEnabled;
}
