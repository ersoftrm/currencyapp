package pl.ersoftrm.currencyapp.customer;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import pl.ersoftrm.currencyapp.customer.port.CustomerActivationUseCase;
import pl.ersoftrm.currencyapp.infra.DomainEvent;
import pl.ersoftrm.currencyapp.infra.DomainEventListener;
import pl.ersoftrm.currencyapp.wallet.WalletCreatedEvent;

@Service
@AllArgsConstructor(access = AccessLevel.PACKAGE)
class CustomerEventListener implements DomainEventListener {

    private final CustomerActivationUseCase useCase;
    @Override
    @EventListener(WalletCreatedEvent.class)
    public void onEvent(final @Qualifier ("walletCreatedEvent") DomainEvent event) {
        useCase.activateCustomer((CustomerIdDto) event.getEventObject());
    }
}
