package pl.ersoftrm.currencyapp.customer;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

import java.math.BigDecimal;

@Data
@Getter
@AllArgsConstructor
public class CustomerIdDto {
    private Long customerId;
    private BigDecimal plnStartBalance;
}
