package pl.ersoftrm.currencyapp.customer;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import org.springframework.format.annotation.NumberFormat;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@Getter
public class CustomerRegisterCommand {
    @NotNull
    private String firstname;
    @NotNull
    private String lastname;
    @Email
    private String username;
    @Size(min = 5, max = 10)
    private String password;
    @NotNull
    @NumberFormat
    private BigDecimal plnStartBalance;
}
