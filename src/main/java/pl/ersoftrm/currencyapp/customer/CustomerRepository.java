package pl.ersoftrm.currencyapp.customer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
interface CustomerRepository extends JpaRepository<Customer,Long> {
    Optional<Customer> findByEmailIgnoreCase(String email);
}
