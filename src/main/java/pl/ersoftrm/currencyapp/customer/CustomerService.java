package pl.ersoftrm.currencyapp.customer;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.ersoftrm.currencyapp.customer.port.CustomerActivationUseCase;
import pl.ersoftrm.currencyapp.customer.port.CustomerUseCase;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Service
@RequiredArgsConstructor
public class CustomerService implements CustomerUseCase, CustomerActivationObserver {

    private final CustomerRepository repository;
    private final PasswordEncoder encoder;
    private final CustomerEventPublisher publisher;
    private final CustomerActivationUseCase activationUseCase;
    private final CompletableFuture<CustomerIdDto> activationFuture = new CompletableFuture<>();


    @Override
    public Optional<CustomerDto> fetchCustomer(final String email) {
        return repository.findByEmailIgnoreCase(email).map(Customer::toCustomerDto);
    }

    @SneakyThrows
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public RegisterResponse create(final CustomerRegisterCommand customerRegisterCommand) {
        if (repository.findByEmailIgnoreCase(customerRegisterCommand.getUsername()).isPresent()) {
            return RegisterResponse.failure("Account already exists");
        }
        Customer customer = repository.save(Customer.register(customerRegisterCommand.getFirstname() , customerRegisterCommand.getLastname(),customerRegisterCommand.getUsername(), encoder.encode(customerRegisterCommand.getPassword())));
        publisher.publish(new CustomerCreatedEvent(new CustomerIdDto(customer.getId(), customerRegisterCommand.getPlnStartBalance())));
        CustomerDto customerDto = customer.toCustomerDto();
        customerDto.setIsEnabled(activationFuture.isDone());
        return RegisterResponse.success(customer.toCustomerDto());
    }

    @Override
    public void onCustomerActivated(final CustomerIdDto dto) {
        activationFuture.complete(dto);
    }
}
