package pl.ersoftrm.currencyapp.customer;

import pl.ersoftrm.currencyapp.infra.GenericResponse;

public class RegisterResponse extends GenericResponse<String,CustomerDto > {

    public RegisterResponse(boolean success, String left, CustomerDto right) {
        super(success, left, right);
    }

    public static RegisterResponse success(CustomerDto right) {
        return new RegisterResponse(true, null, right);
    }

    public static RegisterResponse failure(String left) {
        return new RegisterResponse(false, left, null);
    }

}
