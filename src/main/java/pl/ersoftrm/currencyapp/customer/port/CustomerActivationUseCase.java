package pl.ersoftrm.currencyapp.customer.port;

import pl.ersoftrm.currencyapp.customer.CustomerActivationObserver;
import pl.ersoftrm.currencyapp.customer.CustomerIdDto;

public interface CustomerActivationUseCase {

    void registerObserver(CustomerActivationObserver observer);

    void activateCustomer(CustomerIdDto customerIdDto);

    void notifyObservers(CustomerIdDto customerIdDto);
}
