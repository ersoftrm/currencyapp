package pl.ersoftrm.currencyapp.customer.port;

import pl.ersoftrm.currencyapp.customer.CustomerDto;
import pl.ersoftrm.currencyapp.customer.CustomerRegisterCommand;
import pl.ersoftrm.currencyapp.customer.RegisterResponse;

import java.util.Optional;

public interface CustomerUseCase {
    Optional<CustomerDto> fetchCustomer(String email);
    RegisterResponse create(CustomerRegisterCommand customerRegisterCommand);

}
