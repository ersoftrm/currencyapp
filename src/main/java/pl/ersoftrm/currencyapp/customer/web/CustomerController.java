package pl.ersoftrm.currencyapp.customer.web;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.ersoftrm.currencyapp.customer.CustomerRegisterCommand;
import pl.ersoftrm.currencyapp.customer.port.CustomerUseCase;

@RestController
@RequestMapping("/customer")
@AllArgsConstructor
public class CustomerController {
    private final CustomerUseCase customerUseCase;
    @PostMapping()
    public ResponseEntity<?> register(@Valid @RequestBody CustomerRegisterCommand command) {
        return customerUseCase
                .create(command)
                .handle(
                        entity -> ResponseEntity.accepted().build(),
                        error -> ResponseEntity.badRequest().body(error)
                );
    }

}
