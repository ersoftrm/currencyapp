package pl.ersoftrm.currencyapp.infra;

import java.time.Instant;

public interface DomainEvent<T> {
    Instant occurrenceTime();
   T getEventObject();
}
