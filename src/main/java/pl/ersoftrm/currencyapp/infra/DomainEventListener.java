package pl.ersoftrm.currencyapp.infra;

public interface DomainEventListener {

    void onEvent(DomainEvent event);
}
