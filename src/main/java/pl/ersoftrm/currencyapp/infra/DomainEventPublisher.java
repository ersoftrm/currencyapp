package pl.ersoftrm.currencyapp.infra;

public interface DomainEventPublisher {
    void publish(DomainEvent event);
}
