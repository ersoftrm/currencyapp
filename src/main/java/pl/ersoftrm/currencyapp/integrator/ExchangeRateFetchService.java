package pl.ersoftrm.currencyapp.integrator;


import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ExchangeRateFetchService implements ExchangeRateFetchUseCase {
    private final IntegratorProperties properties;
    private final RestTemplate restTemplate;

    @Override
    public Optional<FetchRateDto> getUsdExchangeRate(final String currency) {
        return fetchExchangeRate(currency);
    }

    private Optional<FetchRateDto> fetchExchangeRate(final String currency) {
        ResponseEntity<FetchRateDto> response = restTemplate.getForEntity(properties.getRateUrl() + currency, FetchRateDto.class);
        if (response.getStatusCode() == HttpStatus.OK) {
            return Optional.ofNullable(response.getBody());
        }
        return Optional.empty();
    }
}
