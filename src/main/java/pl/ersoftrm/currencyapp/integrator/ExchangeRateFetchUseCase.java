package pl.ersoftrm.currencyapp.integrator;

import java.util.Optional;

public interface ExchangeRateFetchUseCase {
    Optional<FetchRateDto> getUsdExchangeRate(String currency);
}
