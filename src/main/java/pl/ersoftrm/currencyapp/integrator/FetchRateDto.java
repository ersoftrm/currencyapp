package pl.ersoftrm.currencyapp.integrator;

import lombok.*;
import pl.ersoftrm.currencyapp.wallet.currency.CurrencyRateDto;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@ToString
public class FetchRateDto {

    private String table;
    private String currency;
    private String code;
    private List<CurrencyRateDto> rates = new ArrayList<>();
}
