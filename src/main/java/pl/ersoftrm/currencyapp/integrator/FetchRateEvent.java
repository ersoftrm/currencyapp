package pl.ersoftrm.currencyapp.integrator;

import pl.ersoftrm.currencyapp.infra.DomainEvent;

import java.time.Instant;

public class FetchRateEvent implements DomainEvent<FetchRateDto> {

    private final Instant occurredOn;
    private final FetchRateDto fetchRateDto;

    public FetchRateEvent(final FetchRateDto fetchRateDto) {
        this.occurredOn = Instant.now();
        this.fetchRateDto = fetchRateDto;
    }

    @Override
    public Instant occurrenceTime() {
        return occurredOn;
    }

    @Override
    public FetchRateDto getEventObject() {
        return fetchRateDto;
    }
}
