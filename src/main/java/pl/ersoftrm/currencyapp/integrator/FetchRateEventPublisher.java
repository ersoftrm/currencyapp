package pl.ersoftrm.currencyapp.integrator;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import pl.ersoftrm.currencyapp.infra.DomainEvent;
import pl.ersoftrm.currencyapp.infra.DomainEventPublisher;

@Service
public class FetchRateEventPublisher implements DomainEventPublisher {

    private final ApplicationEventPublisher publisher;

    FetchRateEventPublisher(final ApplicationEventPublisher publisher) {
        this.publisher = publisher;
    }

    @Override
    public void publish(final DomainEvent event) {
        publisher.publishEvent(event);
    }
}
