package pl.ersoftrm.currencyapp.integrator;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class FetchRatesJob {

    private final ExchangeRateFetchUseCase fetchRateUseCase;
    private final FetchRateEventPublisher publisher;

    @Scheduled(cron = "${app.integrator.cron}")
    public void run() {
        fetchRateUseCase.getUsdExchangeRate("usd").ifPresent(fetchRateDto -> {
            publisher.publish(new FetchRateEvent(fetchRateDto));
        });
    }
}
