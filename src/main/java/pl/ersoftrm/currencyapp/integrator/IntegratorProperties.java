package pl.ersoftrm.currencyapp.integrator;

import lombok.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Value
@ConfigurationProperties("app.integrator")
public class IntegratorProperties {
    String crone;
    String rateUrl;
}
