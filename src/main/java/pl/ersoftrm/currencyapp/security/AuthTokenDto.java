package pl.ersoftrm.currencyapp.security;
public record AuthTokenDto(String authToken) {
}
