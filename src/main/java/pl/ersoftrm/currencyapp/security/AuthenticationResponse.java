package pl.ersoftrm.currencyapp.security;

import pl.ersoftrm.currencyapp.infra.GenericResponse;

public class AuthenticationResponse extends GenericResponse<String, AuthTokenDto> {

    public AuthenticationResponse(boolean success, String left, AuthTokenDto right) {
        super(success, left, right);
    }

    public static AuthenticationResponse success(AuthTokenDto right) {
        return new AuthenticationResponse(true, null, right);
    }

    public static AuthenticationResponse failure(String left) {
        return new AuthenticationResponse(false, left, null);
    }

}
