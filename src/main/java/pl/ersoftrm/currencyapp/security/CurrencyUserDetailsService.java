package pl.ersoftrm.currencyapp.security;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.ersoftrm.currencyapp.customer.port.CustomerUseCase;

@AllArgsConstructor
@Service
public class CurrencyUserDetailsService implements UserDetailsService {

    private final CustomerUseCase customerUseCase;
    private final JwtService jwtService;
    private final AuthenticationConfiguration config;

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        return customerUseCase.fetchCustomer(username)
                .map(CustomerSecurityDetails::new)
                .orElseThrow(() -> new UsernameNotFoundException(username));
    }

    @SneakyThrows
    public AuthenticationResponse authenticate(final CustomerAuthCommand command)  {
        Authentication authentication =  config.getAuthenticationManager().authenticate(new UsernamePasswordAuthenticationToken(command.getUsername(), command.getPassword()));
        if (authentication.isAuthenticated()) {
            return AuthenticationResponse.success(jwtService.generateToken(command.getUsername()));
        }
        return AuthenticationResponse.failure("Account already exists");
    }
}
