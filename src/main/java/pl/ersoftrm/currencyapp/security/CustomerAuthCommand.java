package pl.ersoftrm.currencyapp.security;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerAuthCommand {
    @Email
    private String username;
    @Size(min = 5, max = 10)
    private String password;
}
