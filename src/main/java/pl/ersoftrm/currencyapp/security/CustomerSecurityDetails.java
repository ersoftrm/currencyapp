package pl.ersoftrm.currencyapp.security;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pl.ersoftrm.currencyapp.customer.CustomerDto;

import java.util.Collection;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
class CustomerSecurityDetails implements UserDetails {
    private final CustomerDto dto;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return dto.getPassword();
    }

    @Override
    public String getUsername() {
        return dto.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return dto.getIsEnabled();
    }
}
