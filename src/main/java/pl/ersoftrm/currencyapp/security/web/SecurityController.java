package pl.ersoftrm.currencyapp.security.web;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.ersoftrm.currencyapp.security.CurrencyUserDetailsService;
import pl.ersoftrm.currencyapp.security.CustomerAuthCommand;

@AllArgsConstructor
@RestController
@RequestMapping("/security")
public class SecurityController {

    private final CurrencyUserDetailsService userDetailsService;

    @PostMapping("/authenticate")
    public ResponseEntity<Object> getAuthToken(@RequestBody CustomerAuthCommand command) {
        return userDetailsService
                .authenticate(command)
                .handle(
                        entity -> ResponseEntity.status(HttpStatus.OK).body(entity),
                        error-> ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(error)
                );
    }
}
