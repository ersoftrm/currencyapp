package pl.ersoftrm.currencyapp.wallet;


import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.Table;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import pl.ersoftrm.currencyapp.infra.BaseEntity;

import java.math.BigDecimal;
import java.util.HashSet;

@Entity
@Table(name = "wallet")
@EntityListeners(AuditingEntityListener.class)
class Wallet extends BaseEntity {
    private Long customerId;
    private BigDecimal plnBalance;
    //private final Set<CurrencyAccount> currencyAccounts = new HashSet<>();

    Wallet() {
    }
     Wallet(Long customerId, final BigDecimal plnBalance) {
        this.customerId = customerId;
        this.plnBalance = plnBalance;
    }

    Wallet updateWalletPlnBalance(BigDecimal plnBalance) {
        this.plnBalance = plnBalance;
        return this;
    }

     static Wallet create(Long customerId, BigDecimal plnBalance) {
        return new Wallet(customerId, plnBalance);
    }

   public WalletDto toWalletQuery(){
       return new WalletDto(this.getId(),this.plnBalance,new HashSet<>());
    }
  /* public WalletDto toWalletQuery(){
       return new WalletDto(this.getId(),this.plnBalance,this.currencyAccounts);
    }*/

}
