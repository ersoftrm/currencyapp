package pl.ersoftrm.currencyapp.wallet;

import pl.ersoftrm.currencyapp.customer.CustomerIdDto;
import pl.ersoftrm.currencyapp.infra.DomainEvent;

import java.time.Instant;

public class WalletCreatedEvent implements DomainEvent<CustomerIdDto>
{
    private final Instant occurredOn;
    private final CustomerIdDto dto;
    public WalletCreatedEvent(final CustomerIdDto dto) {
        this.occurredOn = Instant.now();
        this.dto = dto;
    }
    @Override
    public Instant occurrenceTime() {
        return occurredOn;
    }

    @Override
    public CustomerIdDto getEventObject() {
        return dto;
    }
}
