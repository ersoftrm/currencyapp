package pl.ersoftrm.currencyapp.wallet;

import pl.ersoftrm.currencyapp.wallet.account.CurrencyAccount;

import java.math.BigDecimal;
import java.util.Set;


public class WalletDto {
    private final Long walletId;
    private final BigDecimal plnBalance;
  //  private final Set<CurrencyAccount> currencyAccounts;

/*    WalletDto(Long walletId, BigDecimal plnBalance, Set<CurrencyAccount> currencyAccounts) {
        this.walletId = walletId;
        this.plnBalance = plnBalance;
        this.currencyAccounts = currencyAccounts;
    } */
    WalletDto(Long walletId, BigDecimal plnBalance, Set<CurrencyAccount> currencyAccounts) {
        this.walletId = walletId;
        this.plnBalance = plnBalance;
    }
}
