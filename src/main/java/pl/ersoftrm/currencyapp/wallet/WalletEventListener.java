package pl.ersoftrm.currencyapp.wallet;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import pl.ersoftrm.currencyapp.customer.CustomerCreatedEvent;

@Service
@AllArgsConstructor(access = AccessLevel.PACKAGE)
class WalletEventListener {

    private final WalletUseCase useCase;

    @EventListener(CustomerCreatedEvent.class)
    public void onEvent(final CustomerCreatedEvent event) {
        useCase.createWallet(event.getEventObject());
    }
}
