package pl.ersoftrm.currencyapp.wallet;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import pl.ersoftrm.currencyapp.infra.DomainEvent;
import pl.ersoftrm.currencyapp.infra.DomainEventPublisher;

@Service
class WalletEventPublisher implements DomainEventPublisher {

    private final ApplicationEventPublisher publisher;

    WalletEventPublisher(final ApplicationEventPublisher publisher) {
        this.publisher = publisher;
    }

    @Override
    public void publish(final DomainEvent event) {
        publisher.publishEvent(event);
    }
}
