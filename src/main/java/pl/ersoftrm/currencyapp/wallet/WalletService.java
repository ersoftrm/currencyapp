package pl.ersoftrm.currencyapp.wallet;

import org.springframework.stereotype.Service;
import pl.ersoftrm.currencyapp.customer.CustomerIdDto;

@Service
public class WalletService implements WalletUseCase {

    private final WalletRepository repository;
    private final WalletEventPublisher publisher;

    WalletService(final WalletRepository repository, final WalletEventPublisher publisher) {
        this.repository = repository;
        this.publisher = publisher;
    }

    @Override
    public void createWallet(final CustomerIdDto dto) {
        repository.save(Wallet.create(dto.getCustomerId(),dto.getPlnStartBalance()));
        publisher.publish(new WalletCreatedEvent(dto));
    }
}
