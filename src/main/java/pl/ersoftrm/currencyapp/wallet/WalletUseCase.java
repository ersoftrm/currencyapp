package pl.ersoftrm.currencyapp.wallet;

import pl.ersoftrm.currencyapp.customer.CustomerIdDto;

public interface WalletUseCase {

    void createWallet(CustomerIdDto customerId);
}
