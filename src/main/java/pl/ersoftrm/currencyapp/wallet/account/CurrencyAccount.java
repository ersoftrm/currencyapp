package pl.ersoftrm.currencyapp.wallet.account;

import jakarta.persistence.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import pl.ersoftrm.currencyapp.infra.BaseEntity;
import pl.ersoftrm.currencyapp.wallet.currency.Currency;

import java.math.BigDecimal;

@Entity
@Table(name = "currency_account")
@EntityListeners(AuditingEntityListener.class)
public class CurrencyAccount extends BaseEntity {

    @Enumerated(EnumType.STRING)
    Currency currency;
    BigDecimal currencyBalance;

    CurrencyAccount() {
    }
}
