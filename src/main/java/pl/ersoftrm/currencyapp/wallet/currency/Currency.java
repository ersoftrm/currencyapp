package pl.ersoftrm.currencyapp.wallet.currency;

import lombok.Getter;

@Getter
public enum Currency {
    PLN("PLN"),
    USD("USD");

    private final String code;
    Currency(final String code) {
        this.code = code;
    }
}
