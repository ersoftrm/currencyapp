package pl.ersoftrm.currencyapp.wallet.currency;

import jakarta.persistence.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import pl.ersoftrm.currencyapp.infra.BaseEntity;

import java.time.LocalDate;

@Entity
@Table(name = "currency_rate")
@EntityListeners(AuditingEntityListener.class)
class CurrencyRate extends BaseEntity {
    private Float bid;
    private Float ask;
    private String no;
    private LocalDate effectiveDate;
    @Enumerated(value = EnumType.STRING)
    private final Currency currency = Currency.USD;

    protected CurrencyRate(){}

    private CurrencyRate(final Float bid, final Float ask, final String no, final LocalDate effectiveDate) {
        this.bid = bid;
        this.ask = ask;
        this.no = no;
        this.effectiveDate = effectiveDate;
    }

    public static CurrencyRate fromDto(final CurrencyRateDto dto){
        return new CurrencyRate(dto.getBid(), dto.getAsk(), dto.getNo(), dto.getEffectiveDate());
    }
}
