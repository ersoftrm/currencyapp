package pl.ersoftrm.currencyapp.wallet.currency;

import lombok.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@ToString
public class CurrencyRateDto {
    private String no;
    private String effectiveDate;
    private float bid;
    private float ask;

    public LocalDate getEffectiveDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(effectiveDate, formatter);
    }
}
