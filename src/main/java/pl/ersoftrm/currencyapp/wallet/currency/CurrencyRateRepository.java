package pl.ersoftrm.currencyapp.wallet.currency;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

@Repository
interface CurrencyRateRepository extends JpaRepository<CurrencyRate, Long> {
    Optional<CurrencyRate> findFirstByEffectiveDateAndAskAndBid(LocalDate date, BigDecimal ask, BigDecimal bid);
}
