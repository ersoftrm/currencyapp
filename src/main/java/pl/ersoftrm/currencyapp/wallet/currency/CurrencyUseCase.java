package pl.ersoftrm.currencyapp.wallet.currency;

import pl.ersoftrm.currencyapp.integrator.FetchRateDto;

interface CurrencyUseCase {
    void handleCurrencyFetchEvent(FetchRateDto currencyRate);
}
