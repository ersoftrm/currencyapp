package pl.ersoftrm.currencyapp.wallet.currency;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.ersoftrm.currencyapp.integrator.FetchRateDto;

import java.math.BigDecimal;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CurrencyUseCaseService implements CurrencyUseCase {

    private final CurrencyRateRepository repository;

    CurrencyUseCaseService(CurrencyRateRepository repository) {
        this.repository = repository;
    }

    @Override
    @Transactional
    public void handleCurrencyFetchEvent(final FetchRateDto currencyRate) {
        log.info("running handleCurrencyFetchEvent method on " + currencyRate.toString());
        repository.saveAll(currencyRate.getRates().stream()
                .filter(dto -> repository.findFirstByEffectiveDateAndAskAndBid(dto.getEffectiveDate(), BigDecimal.valueOf(dto.getAsk()), BigDecimal.valueOf(dto.getBid())).isEmpty())
                .map(CurrencyRate::fromDto).collect(Collectors.toList()));
    }
}
