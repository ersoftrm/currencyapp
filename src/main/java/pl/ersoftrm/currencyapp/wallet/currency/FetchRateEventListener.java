package pl.ersoftrm.currencyapp.wallet.currency;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import pl.ersoftrm.currencyapp.integrator.FetchRateEvent;

@Service
class FetchRateEventListener  {

    private final CurrencyUseCase useCase;

    FetchRateEventListener(CurrencyUseCase useCase) {
        this.useCase = useCase;
    }

    @EventListener(FetchRateEvent.class)
    public void onEvent(final FetchRateEvent event) {
        useCase.handleCurrencyFetchEvent(event.getEventObject());
    }
}
