package pl.ersoftrm.currencyapp.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/welcome")
public class WelcomeController {

    private final RequestMappingHandlerMapping handlerMapping;

    @Autowired
    public WelcomeController(@Qualifier("requestMappingHandlerMapping") RequestMappingHandlerMapping handlerMapping) {
        this.handlerMapping = handlerMapping;
    }

    @GetMapping()
    public Map<String, String> getEndpoints() {
        Map<String, String> endpoints = new HashMap<>();
        this.handlerMapping.getHandlerMethods().forEach((key, value) -> endpoints.put(key.toString(), value.toString()));
        return endpoints;
    }


}
